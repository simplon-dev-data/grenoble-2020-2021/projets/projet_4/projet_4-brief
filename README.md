![Cover](cover.jpg)

# La vérité est ailleurs

> Analyse des données des cas d'observations recensés par le GEIPAN

## Contexte du projet

Le bureau du GEIPAN au CNES recense les observations de phénomènes non identifiés en France.
Les données des observations sont depuis peu disponible en Open Data et nous souhaitons
exploiter pleinement leur potentiel.

Ce jeu de données pose plusieurs problématiques :

- Trouver des indicateurs permettant de mettre en lumière des explications rationnelles et pertinentes
- Analyser et améliorer la qualité des données pour en faciliter l'exploitation

Objectifs du projet :

- Se focaliser sur un axe principal du cycle de vie de la données
- Découvrir et manipuler de nouveaux outils data
- Préparer une activité d'initiation pour les autres apprenants

## Ressource(s)

- [Site Web du GEIPAN](https://www.cnes-geipan.fr)

## Livrables

- Dépôt GitLab
- Activité d'initiation en mode pédagogie active
- En individuel, quelques slides de retour d'expérience

## Modalités pédagogiques

Le projet est à réaliser par groupe de 2 ou 3 apprenants sur 5 jours :
20/05, 21/05, 25/05, 27/05, 28/05.

Chaque groupe choisit :

- Un domaine de prédilection du cycle de vie de la données (ETL, Qualité ou Analyse)
- Un outil data principal à découvrir

Après prise en main de l'outil, réflechir comment mettre en place une activité
d'initiation en mode pédagogie active (imiter, adapter) pour les autres apprenants.
Vous pouvez vous inspirer des activités proposées par vos formateurs !

Les activités d'initiation animées par chaque groupe s'effectueront par créneaux
d'environ 2 heures du 31/05 au 02/06.

## Critères de performance

Suggestions de nouveaux outils par domaine :

- ETL / Data Warehouse / Orchestration : [Prefect](https://www.prefect.io), [DBT](https://www.getdbt.com)
- Cohérence / Contrôle qualité des données : [GreatExpectations](https://greatexpectations.io)
- Visualisation / Analyse / Exploration de données : [PowerBI](https://powerbi.microsoft.com), [Tableau](https://www.tableau.com), [Datasette](https://datasette.io)

## Modalités d'évaluation

La qualité de votre activité d'initiation pour les autres apprenants attestera
de votre montée en compétence sur les nouveaux outils/concepts mis en oeuvre.

Egalement, nous souhaitons que chaque apprenant réalise quelques slides (maximum 5)
de présentation avec Google Slides récapitulant votre retour d'expérience sur :

- Les nouveaux outils pris en main
- La mise en place d'une activité en pédagogie active
